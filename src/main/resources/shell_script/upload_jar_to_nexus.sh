mvn deploy:deploy-file \
    -Durl=${repositoryUrl} \
    -DrepositoryId=${repositoryId} \
    -DgroupId={group} \
    -DartifactId={projectName} \
    -Dversion=${version}  \
    -Dpackaging=${packaging} \
    -Dfile=${filePath}
