package com.tw.repository.api;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@RestController
public class NexusApi {

    @RequestMapping(value = "/repository")
    public ResponseEntity<?> findComponentsOfRepository(HttpServletRequest request) throws IOException {
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8081/service/siesta/rest/beta/components?repositoryId={repositoryId}";
        Map<String, String> map = new HashMap<>();
        map.put("repositoryId", "scaleRepository");
//        map.put("repositoryId", request.getParameter("repositoryId"));
        RepositoryResponse forObject = restTemplate.getForObject(url, RepositoryResponse.class, map);
        return new ResponseEntity<>(forObject, HttpStatus.OK) ;
    }

    @RequestMapping(value = "/deploy", method = {RequestMethod.POST})
    public ResponseEntity<String> deployProject(@RequestBody HashMap<String, String> repository) throws IOException {
//        String projectZipName = createProject.createProject(projectEntity);
        invokeRundeckJob(repository.get("name"));
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    public void invokeRundeckJob(String projectName) {

        MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
        map.add("asUser", "admin");
        map.add("argString", "-project_name "+projectName);
        Map<String, String> operationMap = new HashMap<>();
        operationMap.put("project_name", projectName);
        map.add("options", "admin");


        RundeckJobEntry rundeckJobEntry = new RundeckJobEntry("admin", operationMap);
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Rundeck-Auth-Token", "AfMT4jryQAs7av1WEfwJ3CTTWAxFzV5m");

        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<?> entity = new HttpEntity(rundeckJobEntry, headers);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
        RestTemplate restTemplate = new RestTemplate();

        restTemplate.postForObject("http://localhost:4440/api/19/job/9a865428-1b3c-424f-8cbb-71471ecaab11/executions", entity, Map.class);

//        try {
//            RestTemplate restTemplate = new RestTemplate();
//            new Thread(() -> {
//                restTemplate.postForObject("http://localhost:4440/api/19/job/9a865428-1b3c-424f-8cbb-71471ecaab11/executions", entity, Map.class);
////                restTemplate.postForObject("http://localhost:9097/project_test", request, Map.class);
////                restTemplate.exchange("http://localhost:4440/api/7/job/9a865428-1b3c-424f-8cbb-71471ecaab11/run", HttpMethod.POST, request, String.class);
//
//            }).start();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }



    }

    private static class RundeckJobEntry{
        String asUser;
        Map<String, String> options;

        public RundeckJobEntry(String asUser, Map<String, String> options) {
            this.asUser = asUser;
            this.options = options;
        }

        public String getAsUser() {
            return asUser;
        }

        public RundeckJobEntry setAsUser(String asUser) {
            this.asUser = asUser;
            return this;
        }

        public Map<String, String> getOptions() {
            return options;
        }

        public RundeckJobEntry setOptions(Map<String, String> options) {
            this.options = options;
            return this;
        }
    }
}
