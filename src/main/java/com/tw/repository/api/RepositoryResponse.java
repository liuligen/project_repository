package com.tw.repository.api;

import java.util.List;

public class RepositoryResponse {


    /**
     * items : [{"id":"c2NhbGVSZXBvc2l0b3J5Ojg4NDkxY2QxZDE4NWRkMTM2MzI2OGYyMjBlNDVkN2Rl","group":"org.myorg","name":"myproj","version":"1.2.3","repository":"scaleRepository","format":"maven2","assets":[{"downloadUrl":"http://localhost:8081/repository/scaleRepository/org/myorg/myproj/1.2.3/myproj-1.2.3.jar","path":"org/myorg/myproj/1.2.3/myproj-1.2.3.jar","id":"c2NhbGVSZXBvc2l0b3J5OjNmNWNhZTAxNzYwMjMzYjYzMDg5OGJmNmZlMWQ5MTY0","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/org/myorg/myproj/1.2.3/myproj-1.2.3.jar.md5","path":"org/myorg/myproj/1.2.3/myproj-1.2.3.jar.md5","id":"c2NhbGVSZXBvc2l0b3J5OjIxMDMxZGZhZjQ1ZTViNTgzNjE1NGJhYTBkYzIxZjBj","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/org/myorg/myproj/1.2.3/myproj-1.2.3.jar.sha1","path":"org/myorg/myproj/1.2.3/myproj-1.2.3.jar.sha1","id":"c2NhbGVSZXBvc2l0b3J5OmY4OThiMzkwM2NiOTljNTk2ODM1OTdlZGM1YWZmOWJj","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/org/myorg/myproj/1.2.3/myproj-1.2.3.pom","path":"org/myorg/myproj/1.2.3/myproj-1.2.3.pom","id":"c2NhbGVSZXBvc2l0b3J5OjU4MTgwMDVkZTJlYjJiZDEwODFmYjVlMWRkYWFmMjNj","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/org/myorg/myproj/1.2.3/myproj-1.2.3.pom.md5","path":"org/myorg/myproj/1.2.3/myproj-1.2.3.pom.md5","id":"c2NhbGVSZXBvc2l0b3J5OmQwNjQ4MDRhOGVkNWFkNmU2OGZkZTkxY2YzY2JlODMz","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/org/myorg/myproj/1.2.3/myproj-1.2.3.pom.sha1","path":"org/myorg/myproj/1.2.3/myproj-1.2.3.pom.sha1","id":"c2NhbGVSZXBvc2l0b3J5OmJiNmM2ZWY5Nzk2ZGFjODM4MWNjOWI2OTliZTRmMGJl","repository":"scaleRepository","format":"maven2"}]},{"id":"c2NhbGVSZXBvc2l0b3J5OjZkZmRhMGE4NDNmNDIwMWQ3YzgzMTczMWIyNjA5YWVl","group":"com.tw","name":"cloud_demo","version":"v12","repository":"scaleRepository","format":"maven2","assets":[{"downloadUrl":"http://localhost:8081/repository/scaleRepository/com/tw/cloud_demo/v12/cloud_demo-v12.jar","path":"com/tw/cloud_demo/v12/cloud_demo-v12.jar","id":"c2NhbGVSZXBvc2l0b3J5OjIxMDMxZGZhZjQ1ZTViNTg0YTI4NjgxOWZmNmVjMjcw","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/com/tw/cloud_demo/v12/cloud_demo-v12.jar.md5","path":"com/tw/cloud_demo/v12/cloud_demo-v12.jar.md5","id":"c2NhbGVSZXBvc2l0b3J5OmJiNmM2ZWY5Nzk2ZGFjODNkZGMwM2UxMGM4ODMzNjM0","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/com/tw/cloud_demo/v12/cloud_demo-v12.jar.sha1","path":"com/tw/cloud_demo/v12/cloud_demo-v12.jar.sha1","id":"c2NhbGVSZXBvc2l0b3J5OjU4MTgwMDVkZTJlYjJiZDEyZmU0OWMxZjYzOTdkNDc2","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/com/tw/cloud_demo/v12/cloud_demo-v12.pom","path":"com/tw/cloud_demo/v12/cloud_demo-v12.pom","id":"c2NhbGVSZXBvc2l0b3J5OmQwNjQ4MDRhOGVkNWFkNmU1OTg5ZGNlYzI3ZDQwYjQz","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/com/tw/cloud_demo/v12/cloud_demo-v12.pom.md5","path":"com/tw/cloud_demo/v12/cloud_demo-v12.pom.md5","id":"c2NhbGVSZXBvc2l0b3J5OjY1NGJiN2QwYTU5MjEzODU5MDFhYjg3OWQ3ZDc0ODg3","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/com/tw/cloud_demo/v12/cloud_demo-v12.pom.sha1","path":"com/tw/cloud_demo/v12/cloud_demo-v12.pom.sha1","id":"c2NhbGVSZXBvc2l0b3J5OmUwMTg4ZWQwNzI4ZmE2OGYxMDljMWYxYzFiMGQ3NWI0","repository":"scaleRepository","format":"maven2"}]},{"id":"c2NhbGVSZXBvc2l0b3J5OmYxMGJkMDU5M2RlM2I1ZTRkMmY2OTViNGZhODFlNTFi","group":"com.tw.frond","name":"cloud_demo","version":"v1.1","repository":"scaleRepository","format":"maven2","assets":[{"downloadUrl":"http://localhost:8081/repository/scaleRepository/com/tw/frond/cloud_demo/v1.1/cloud_demo-v1.1.pom","path":"com/tw/frond/cloud_demo/v1.1/cloud_demo-v1.1.pom","id":"c2NhbGVSZXBvc2l0b3J5OjU4MTgwMDVkZTJlYjJiZDE5YmNhMTQ0ZDM2ZDBmZjUw","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/com/tw/frond/cloud_demo/v1.1/cloud_demo-v1.1.pom.md5","path":"com/tw/frond/cloud_demo/v1.1/cloud_demo-v1.1.pom.md5","id":"c2NhbGVSZXBvc2l0b3J5OmQwNjQ4MDRhOGVkNWFkNmU4NmEyNTdlMThjOTUyYjQ0","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/com/tw/frond/cloud_demo/v1.1/cloud_demo-v1.1.pom.sha1","path":"com/tw/frond/cloud_demo/v1.1/cloud_demo-v1.1.pom.sha1","id":"c2NhbGVSZXBvc2l0b3J5OmJiNmM2ZWY5Nzk2ZGFjODNhNGMzOGU2OWIzM2M2YjFj","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/com/tw/frond/cloud_demo/v1.1/cloud_demo-v1.1.zip","path":"com/tw/frond/cloud_demo/v1.1/cloud_demo-v1.1.zip","id":"c2NhbGVSZXBvc2l0b3J5OjNmNWNhZTAxNzYwMjMzYjZjM2NiMTFmODIzNDlmNGQz","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/com/tw/frond/cloud_demo/v1.1/cloud_demo-v1.1.zip.md5","path":"com/tw/frond/cloud_demo/v1.1/cloud_demo-v1.1.zip.md5","id":"c2NhbGVSZXBvc2l0b3J5OjIxMDMxZGZhZjQ1ZTViNTgzOTZlYjE3M2RhZmI5ZmI2","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/com/tw/frond/cloud_demo/v1.1/cloud_demo-v1.1.zip.sha1","path":"com/tw/frond/cloud_demo/v1.1/cloud_demo-v1.1.zip.sha1","id":"c2NhbGVSZXBvc2l0b3J5OmY4OThiMzkwM2NiOTljNTkxMmVjMzZlN2ZlZDhkNTQ3","repository":"scaleRepository","format":"maven2"}]},{"id":"c2NhbGVSZXBvc2l0b3J5OjQwMjkyYWNkZWJjMDFiODMxMDMwY2Q1MjdlZDZmNDRj","group":"com.tw","name":"huluwa","version":"1510243334542","repository":"scaleRepository","format":"maven2","assets":[{"downloadUrl":"http://localhost:8081/repository/scaleRepository/com/tw/huluwa/1510243334542/huluwa-1510243334542.jar","path":"com/tw/huluwa/1510243334542/huluwa-1510243334542.jar","id":"c2NhbGVSZXBvc2l0b3J5OmJiNmM2ZWY5Nzk2ZGFjODNjMzc2Nzg3ODczMGE2Mzcx","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/com/tw/huluwa/1510243334542/huluwa-1510243334542.jar.md5","path":"com/tw/huluwa/1510243334542/huluwa-1510243334542.jar.md5","id":"c2NhbGVSZXBvc2l0b3J5OmUwMTg4ZWQwNzI4ZmE2OGY0MmM2OTY2Zjg5NmQ3MTBm","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/com/tw/huluwa/1510243334542/huluwa-1510243334542.jar.sha1","path":"com/tw/huluwa/1510243334542/huluwa-1510243334542.jar.sha1","id":"c2NhbGVSZXBvc2l0b3J5OmQwNjQ4MDRhOGVkNWFkNmUwOGVjZTA0M2U0ZjQyZmI3","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/com/tw/huluwa/1510243334542/huluwa-1510243334542.pom","path":"com/tw/huluwa/1510243334542/huluwa-1510243334542.pom","id":"c2NhbGVSZXBvc2l0b3J5OjY1NGJiN2QwYTU5MjEzODUyZDIxMjUyODdmYzE3ZDI3","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/com/tw/huluwa/1510243334542/huluwa-1510243334542.pom.md5","path":"com/tw/huluwa/1510243334542/huluwa-1510243334542.pom.md5","id":"c2NhbGVSZXBvc2l0b3J5OmY4OThiMzkwM2NiOTljNTljZmM5YjFkMmJiN2VhMWRm","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/com/tw/huluwa/1510243334542/huluwa-1510243334542.pom.sha1","path":"com/tw/huluwa/1510243334542/huluwa-1510243334542.pom.sha1","id":"c2NhbGVSZXBvc2l0b3J5OjNmNWNhZTAxNzYwMjMzYjZiMzNlMzhmY2E4OWJiNTgz","repository":"scaleRepository","format":"maven2"}]},{"id":"c2NhbGVSZXBvc2l0b3J5OjJlNDdkZGEwZjFiNTU1ZTA3MTU5ZGM5ZjlkZDNmZWY0","group":"com.tw","name":"huluwa","version":"1510556862769","repository":"scaleRepository","format":"maven2","assets":[{"downloadUrl":"http://localhost:8081/repository/scaleRepository/com/tw/huluwa/1510556862769/huluwa-1510556862769.jar","path":"com/tw/huluwa/1510556862769/huluwa-1510556862769.jar","id":"c2NhbGVSZXBvc2l0b3J5OmQwNjQ4MDRhOGVkNWFkNmU2ODZhNTQzMGI5OGEyMjFj","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/com/tw/huluwa/1510556862769/huluwa-1510556862769.jar.md5","path":"com/tw/huluwa/1510556862769/huluwa-1510556862769.jar.md5","id":"c2NhbGVSZXBvc2l0b3J5OjY1NGJiN2QwYTU5MjEzODUxOGM3NDc1ZTcxN2FiZGIw","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/com/tw/huluwa/1510556862769/huluwa-1510556862769.jar.sha1","path":"com/tw/huluwa/1510556862769/huluwa-1510556862769.jar.sha1","id":"c2NhbGVSZXBvc2l0b3J5OmUwMTg4ZWQwNzI4ZmE2OGYyZTJmZDNlOTBiOTAwYTQ0","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/com/tw/huluwa/1510556862769/huluwa-1510556862769.pom","path":"com/tw/huluwa/1510556862769/huluwa-1510556862769.pom","id":"c2NhbGVSZXBvc2l0b3J5OjNmNWNhZTAxNzYwMjMzYjYwZmY4MzkwZDQ3ZmE0MGI4","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/com/tw/huluwa/1510556862769/huluwa-1510556862769.pom.md5","path":"com/tw/huluwa/1510556862769/huluwa-1510556862769.pom.md5","id":"c2NhbGVSZXBvc2l0b3J5OjIxMDMxZGZhZjQ1ZTViNTg0OWZiNzU5NTc0NjYyY2Ew","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/com/tw/huluwa/1510556862769/huluwa-1510556862769.pom.sha1","path":"com/tw/huluwa/1510556862769/huluwa-1510556862769.pom.sha1","id":"c2NhbGVSZXBvc2l0b3J5OmY4OThiMzkwM2NiOTljNTllZjQyYTUxYmQ0M2JlY2Iz","repository":"scaleRepository","format":"maven2"}]}]
     * continuationToken : null
     */

    private Object continuationToken;
    private List<ItemsBean> items;

    public Object getContinuationToken() {
        return continuationToken;
    }

    public void setContinuationToken(Object continuationToken) {
        this.continuationToken = continuationToken;
    }

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public static class ItemsBean {
        /**
         * id : c2NhbGVSZXBvc2l0b3J5Ojg4NDkxY2QxZDE4NWRkMTM2MzI2OGYyMjBlNDVkN2Rl
         * group : org.myorg
         * name : myproj
         * version : 1.2.3
         * repository : scaleRepository
         * format : maven2
         * assets : [{"downloadUrl":"http://localhost:8081/repository/scaleRepository/org/myorg/myproj/1.2.3/myproj-1.2.3.jar","path":"org/myorg/myproj/1.2.3/myproj-1.2.3.jar","id":"c2NhbGVSZXBvc2l0b3J5OjNmNWNhZTAxNzYwMjMzYjYzMDg5OGJmNmZlMWQ5MTY0","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/org/myorg/myproj/1.2.3/myproj-1.2.3.jar.md5","path":"org/myorg/myproj/1.2.3/myproj-1.2.3.jar.md5","id":"c2NhbGVSZXBvc2l0b3J5OjIxMDMxZGZhZjQ1ZTViNTgzNjE1NGJhYTBkYzIxZjBj","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/org/myorg/myproj/1.2.3/myproj-1.2.3.jar.sha1","path":"org/myorg/myproj/1.2.3/myproj-1.2.3.jar.sha1","id":"c2NhbGVSZXBvc2l0b3J5OmY4OThiMzkwM2NiOTljNTk2ODM1OTdlZGM1YWZmOWJj","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/org/myorg/myproj/1.2.3/myproj-1.2.3.pom","path":"org/myorg/myproj/1.2.3/myproj-1.2.3.pom","id":"c2NhbGVSZXBvc2l0b3J5OjU4MTgwMDVkZTJlYjJiZDEwODFmYjVlMWRkYWFmMjNj","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/org/myorg/myproj/1.2.3/myproj-1.2.3.pom.md5","path":"org/myorg/myproj/1.2.3/myproj-1.2.3.pom.md5","id":"c2NhbGVSZXBvc2l0b3J5OmQwNjQ4MDRhOGVkNWFkNmU2OGZkZTkxY2YzY2JlODMz","repository":"scaleRepository","format":"maven2"},{"downloadUrl":"http://localhost:8081/repository/scaleRepository/org/myorg/myproj/1.2.3/myproj-1.2.3.pom.sha1","path":"org/myorg/myproj/1.2.3/myproj-1.2.3.pom.sha1","id":"c2NhbGVSZXBvc2l0b3J5OmJiNmM2ZWY5Nzk2ZGFjODM4MWNjOWI2OTliZTRmMGJl","repository":"scaleRepository","format":"maven2"}]
         */

        private String id;
        private String group;
        private String name;
        private String version;
        private String repository;
        private String format;
        private List<AssetsBean> assets;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getGroup() {
            return group;
        }

        public void setGroup(String group) {
            this.group = group;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public String getRepository() {
            return repository;
        }

        public void setRepository(String repository) {
            this.repository = repository;
        }

        public String getFormat() {
            return format;
        }

        public void setFormat(String format) {
            this.format = format;
        }

        public List<AssetsBean> getAssets() {
            return assets;
        }

        public void setAssets(List<AssetsBean> assets) {
            this.assets = assets;
        }

        public static class AssetsBean {
            /**
             * downloadUrl : http://localhost:8081/repository/scaleRepository/org/myorg/myproj/1.2.3/myproj-1.2.3.jar
             * path : org/myorg/myproj/1.2.3/myproj-1.2.3.jar
             * id : c2NhbGVSZXBvc2l0b3J5OjNmNWNhZTAxNzYwMjMzYjYzMDg5OGJmNmZlMWQ5MTY0
             * repository : scaleRepository
             * format : maven2
             */

            private String downloadUrl;
            private String path;
            private String id;
            private String repository;
            private String format;

            public String getDownloadUrl() {
                return downloadUrl;
            }

            public void setDownloadUrl(String downloadUrl) {
                this.downloadUrl = downloadUrl;
            }

            public String getPath() {
                return path;
            }

            public void setPath(String path) {
                this.path = path;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getRepository() {
                return repository;
            }

            public void setRepository(String repository) {
                this.repository = repository;
            }

            public String getFormat() {
                return format;
            }

            public void setFormat(String format) {
                this.format = format;
            }
        }
    }
}
