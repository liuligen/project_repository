package com.tw.repository.service;

import com.tw.repository.util.ExecuteShell;
import org.springframework.stereotype.Service;

@Service
public class NexusRepository {

    private void compressProject(String projectName) {

        int exitValue = ExecuteShell.executeShell("shell_script/upload_jar_to_nexus.sh", projectName);
        if (0 != exitValue) {
            throw new RuntimeException("");
        }
    }

}
