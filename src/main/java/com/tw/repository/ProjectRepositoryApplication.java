package com.tw.repository;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectRepositoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectRepositoryApplication.class, args);
	}
}
