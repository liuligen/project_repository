- NexusApi类中"X-Rundeck-Auth-Token"请设置为Rundeck中的值，该值需要预先在Rundeck的个人设置中生成
- 要现在Rundeck中创建一个job，当前是为了演示，就是打印出穿过去的真实项目名
Rundeck 做如下操作：
1. 创建一个job
2. 在job中设置一个参数，名称叫project_name
3. rundeck使用admin用户名登陆
4. NexusApi中"http://localhost:4440/api/19/job/9a865428-1b3c-424f-8cbb-71471ecaab11/executions"这一段字符串中的9a865428-1b3c-424f-8cbb-71471ecaab11就是rundeck中job的ID，请替换